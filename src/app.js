import express from 'express';
import 'module-alias/register';
import 'dotenv/config';
import 'colors';
import api from '@routes';
import validationErrors from '@middlewares/validationErrors';

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api', api);
app.use(validationErrors());
const { PORT } = process.env;

// eslint-disable-next-line no-console
app.listen(PORT, () => console.log(`Server is up on this port ${PORT}`.blue));
