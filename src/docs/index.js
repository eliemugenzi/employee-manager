import employeePath from './paths/employees';
import SigninSchema from './definitions/SigninSchema';
import ErrorResponse from './definitions/ErrorResponses';

export default {
  swagger: '2.0',
  info: {
    version: '1.0.0',
    title: 'Employee Management system',
    description: 'Employee Management System, Awesomity Challenge',
  },
  basePath: '/api/v1',
  produces: ['application/json'],
  comsumes: ['application/json'],
  paths: {
    ...employeePath
  },
  definitions: {
    ...SigninSchema,
    ...ErrorResponse
  }
};
