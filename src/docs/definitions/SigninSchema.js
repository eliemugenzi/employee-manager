
export default {
  SigninSchema: {
    type: 'object',
    required: ['email', 'password'],
    properties: {
      email: {
        type: 'string',
        example: 'elie@me.com'
      },
      password: {
        type: 'string'
      }
    }
  }
};
