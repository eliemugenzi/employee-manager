import responses from '../constants/responses';

export default {
  '/auth/signin': {
    post: {
      tags: ['auth'],
      summary: 'User signin',
      parameters: [
        {
          in: 'body',
          name: 'body',
          required: true,
          schema: {
            $ref: '#/definitions/SigninSchema'
          }
        }
      ],
      responses
    }
  }
};
