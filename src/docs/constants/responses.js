export default {
  400: {
    description: 'Bad Request',
    schema: {
      $ref: '#/definitions/ValidationResponse',
    },
  },
  401: {
    description: 'Unauthorized access',
    example: {
      status: 401,
      message: 'Unauthorized Access',
    },
  },
  404: {
    description: 'Record does not exist',
    schema: {
      $ref: '#/definitions/ValidationResponse',
    },
  },
  500: {
    description: 'Internal Server Error',
    example: {
      status: 500,
      message: 'Internal Server Error',
    },
  },
  200: {
    description: 'Successful response',
    schema: {
      $ref: '#/definitions/ValidationResponse'
    }
  }

};
