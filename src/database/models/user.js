
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    national_id: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    email: DataTypes.STRING,
    date_of_birth: DataTypes.DATE,
    status: DataTypes.STRING,
    position: DataTypes.STRING,
    password: DataTypes.STRING,
    verified: DataTypes.BOOLEAN
  }, {});
  // User.associate = function (models) {
  //   // associations can be defined here
  // };
  return User;
};
