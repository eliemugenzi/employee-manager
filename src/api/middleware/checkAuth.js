
import Tokenizer from '@helpers/Token.helper';
import { User } from '@models';

/**
 * @class Auth
 */
class Auth {
  /**
     *
     * @param {Object} req Request object
     * @param {*} res Response object
     * @param {*} next Next function
     * @returns {Object} Response object
     */
  static async verifyToken(req, res, next) {
    const bearerToken = req.headers.authorization;

    if (!bearerToken) {
      return res.status(401).json({
        status: 401,
        message: 'You are not authenticated'
      });
    }

    const token = bearerToken.split(' ')[1];


    const payload = await Tokenizer.decodeToken(token);
    if (!payload) {
      return res.status(401).json({
        status: 401,
        message: 'Invalid token',
        token
      });
    }

    const userFilter = {
      where: {
        email: payload
      }
    };
    const user = await User.findOne(userFilter);
    if (!user) {
      return res.status(400).json({
        status: 400,
        message: 'The user you are authenticating with does not exist'
      });
    }
    req.user = user;
    next();
  }

  /**
   *
   * @param {Object} req Request object
   * @param {Object} res Response object
   * @param {Function} next Next Function
   * @returns {Object} Response object
   */
  static async checkPosition(req, res, next) {
    const { user } = req;

    if (user.position !== 'manager') {
      return res.status(403).json({
        status: 403,
        message: 'You are not allowed to perform this operation'
      });
    }

    next();
  }
}


export default Auth;
