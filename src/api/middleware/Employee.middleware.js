import { User } from '@models';

export const employeeExists = async (req, res, next) => {
  const { params } = req;

  const user = await User.findOne({
    where: {
      id: params.employee_id
    }
  });

  if (!user) {
    return res.status(404).json({
      status: 404,
      message: `User with ID ${params.employee_id} does not exist`
    });
  }

  req.employee = user;

  next();
};

export const isEmployeeActive = async (req, res, next) => {
  const { employee } = req;
  if (employee.status !== 'active') {
    return next();
  }

  return res.status(400).json({
    status: 400,
    message: 'This employee is already active'
  });
};


export const isEmployeeInactive = async (req, res, next) => {
  const { employee } = req;
  if (employee.status === 'active') {
    return next();
  }

  return res.status(400).json({
    status: 400,
    message: 'This employee is already inactive'
  });
};
