
const isProduction = process.env.NODE_ENV === 'production';
const asyncHandler = (cb) => async (req, res, next) => {
  try {
    await cb(req, res, next);
  } catch (err) {
    if (!isProduction) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
    return res.status(500).json({
      status: 500,
      message: err.message || err.data.errorMessage,
      error: !isProduction ? err : undefined,
    });
  }
};

export default asyncHandler;
