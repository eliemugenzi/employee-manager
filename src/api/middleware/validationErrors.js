import { isCelebrate } from 'celebrate';


const joiErrors = () => (err, req, res, next) => {
  if (!isCelebrate(err)) return next(err);
  return res.status(400).json({
    status: 400,
    error: err.joi.details[0].context.label || undefined,
  });
};

export default joiErrors;
