import Tokenizer from '@helpers/Token.helper';

const isTokenValid = async (req, res, next) => {
  const { token } = req.query;
  const payload = await Tokenizer.decodeToken(token);
  if (!payload) {
    return res.status(400).json({
      status: 400,
      message: 'Invalid request'
    });
  }

  next();
};

export default isTokenValid;
