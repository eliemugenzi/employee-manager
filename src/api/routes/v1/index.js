import { Router } from 'express';
import SwaggerUI from 'swagger-ui-express';
import SwaggerDoc from '@docs';
import auth from './Auth.route';
import employees from './Employee.route';

const router = Router();

router.use('/docs', SwaggerUI.serve, SwaggerUI.setup(SwaggerDoc));
router.use('/auth', auth);
router.use('/employees', employees);


export default router;
