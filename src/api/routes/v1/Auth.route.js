import { Router } from 'express';
import { celebrate } from 'celebrate';

import * as authValidator from '@helpers/schemas/Auth.schema';
import AuthController from '@controllers/Auth.controller';
import asyncHandler from '@middlewares/AsyncHandler';
import isTokenValid from '@middlewares/isTokenValid';

const { register, verifyAccount, login } = AuthController;

const router = Router();

router.post('/register',
  celebrate({
    body: authValidator.signupRule
  }),
  asyncHandler(register));

router.get('/verify', asyncHandler(isTokenValid), asyncHandler(verifyAccount));
router.post('/login',
  celebrate({
    body: authValidator.loginRule
  }),
  asyncHandler(login));

export default router;
