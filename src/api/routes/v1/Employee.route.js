import { Router } from 'express';
import { celebrate } from 'celebrate';
import * as employeeValidator from '@helpers/schemas/Employee.schema';
import asyncHandler from '@middlewares/AsyncHandler';
import AuthMiddleware from '@middlewares/checkAuth';
import EmployeeController from '@controllers/Employee.controller';
import { employeeExists, isEmployeeActive, isEmployeeInactive } from '@middlewares/Employee.middleware';

const { verifyToken, checkPosition } = AuthMiddleware;

const {
  createOne, deleteOne, updateOne, activateOne, suspendOne, search
} = EmployeeController;

const router = Router();

router.post('/',
  asyncHandler(verifyToken),
  asyncHandler(checkPosition),
  celebrate({
    body: employeeValidator.employeeCreationRule
  }),
  asyncHandler(createOne));

router.post('/search',
  asyncHandler(verifyToken),
  asyncHandler(search));

router.route('/:employee_id')
  .all(
    asyncHandler(verifyToken),
    asyncHandler(checkPosition),
    asyncHandler(employeeExists),
    celebrate({
      params: employeeValidator.employeeByIdRule
    }),
  )
  .delete(
    asyncHandler(deleteOne)
  )
  .put(
    celebrate({
      body: employeeValidator.employeeUpdataRule
    }),
    asyncHandler(updateOne)
  );

router.put('/:employee_id/activate',
  asyncHandler(verifyToken),
  asyncHandler(checkPosition),
  celebrate({
    params: employeeValidator.employeeByIdRule
  }),
  asyncHandler(employeeExists),
  asyncHandler(isEmployeeActive),
  asyncHandler(activateOne),);

router.put('/:employee_id/suspend',
  celebrate({
    params: employeeValidator.employeeByIdRule
  }),
  asyncHandler(employeeExists),
  asyncHandler(isEmployeeInactive),
  asyncHandler(suspendOne));

export default router;
