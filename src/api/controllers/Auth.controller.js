/* eslint-disable camelcase */
import { User } from '@models';
import { Op } from 'sequelize';
import moment from 'moment';

import HashHelper from '@helpers/Password.helper';
import Mailer from '@helpers/Mail.helper';
import confirmAccountTemplate from '@helpers/emailTemplates/confirmAccount.template';
import Tokenizer from '@helpers/Token.helper';

/**
 * @class AuthController
 */
class AuthController {
  /**
     *
     * @param {Object} req Request Object
     * @param {Object} res Response object
     * @returns {Object} Response object
     */
  static async register(req, res) {
    const {
      email,
      phone_number: phoneNumber,
      password,
      national_id: nationalId,
      name
    } = req.body;

    const code_250 = req.body.phone_number.split('-')[0];
    if (code_250 !== '250') {
      return res.status(400).json({
        status: 400,
        message: 'The number should be a Rwandan one'
      });
    }

    const bornDate = new Date(req.body.date_of_birth);
    const today = new Date();

    const years = moment(today).diff(moment(bornDate), 'years');
    if (parseInt(years, 10) < 18) {
      return res.status(400).json({
        status: 400,
        message: 'Unfortunately, this system only accepts people with at least 18 years old'
      });
    }


    const user = await User.findOne({
      where: {
        [Op.or]: [
          {
            email,
          },
          {
            phone_number: phoneNumber
          },
          {
            national_id: `${nationalId}`
          }
        ]
      }
    });

    if (user) {
      return res.status(409).json({
        status: 409,
        message: 'User with either this email, this phone number or this National ID already exists'
      });
    }

    const newUser = await User.create({
      ...req.body,
      password: HashHelper.hashPassword(password),
      status: 'active',
      position: 'manager',
      national_id: `${nationalId}`
    });

    const token = await Tokenizer.generateToken(newUser.email);

    const mailer = new Mailer({
      to: email,
      html: confirmAccountTemplate({
        name,
        email,
        token
      }),
      subject: 'Welcome'
    });

    await mailer.send();
    res.json({
      status: 200,
      data: newUser
    });
  }

  /**
   *
   * @param {Object} req - Request object
   * @param {Object} res - Response object
   * @returns {Object} Response object
   */
  static async verifyAccount(req, res) {
    const { query } = req;

    const payload = await Tokenizer.decodeToken(query.token);

    const user = await User.update({
      verifed: true,
    }, {
      where: {
        email: payload,
        verified: false
      }
    });

    if (!user) {
      return res.status(404).json({
        status: 404,
        message: 'Account not found'
      });
    }

    return res.status(202).json({
      status: 202,
      message: 'User is now verified'
    });
  }

  /**
   *
   * @param {Object} req - Request Object
   * @param {Object} res - Response object
   * @returns {Object} Response object
   */
  static async login(req, res) {
    const { email, password } = req.body;
    const userFilter = {
      where: {
        email,
      }
    };

    const user = await User.findOne(userFilter);
    if (!user) {
      return res.status(400).json({
        status: 400,
        message: 'Invalid Email and password'
      });
    }

    const userFound = await HashHelper.comparePassword(password, user.password);
    if (!userFound) {
      return res.status(400).json({
        status: 400,
        message: 'Invalid Email and password'
      });
    }

    const token = await Tokenizer.generateToken(user.email);
    res.json({
      status: 200,
      token
    });
  }
}

export default AuthController;
