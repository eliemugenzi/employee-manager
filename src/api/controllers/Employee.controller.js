/* eslint-disable camelcase */
import { User } from '@models';
import moment from 'moment';
import { Op } from 'sequelize';
import informNewEmployee from '@helpers/emailTemplates/employeeJoined.tenplate';
import getRandomInt from '@helpers/randomNumber.helper';
import HashHelper from '@helpers/Password.helper';
import Mailer from '@helpers/Mail.helper';

/**
 * @class EmployeeController
 */
class EmployeeController {
  /**
     *
     * @param {Object} req Request object
     * @param {Object} res response object
     * @returns {Object} Response
     */
  static async createOne(req, res) {
    const { body } = req;
    const where = {
      where: {
        [Op.or]: [
          {
            email: body.email
          },
          {
            phone_number: body.phone_number
          },
          {
            national_id: `${body.national_id}`
          }
        ]
      }
    };

    const code_250 = body.phone_number.split('-')[0];
    if (code_250 !== '250') {
      return res.status(400).json({
        status: 400,
        message: 'The number should be a Rwandan one'
      });
    }


    const bornDate = new Date(req.body.date_of_birth);
    const today = new Date();

    const years = moment(today).diff(moment(bornDate), 'years');
    if (parseInt(years, 10) < 18) {
      return res.status(400).json({
        status: 400,
        message: 'Unfortunately, this system only accepts people with at least 18 years old'
      });
    }
    const user = await User.findOne(where);
    if (user) {
      return res.status(409).json({
        status: 409,
        message: 'Employee already exists'
      });
    }

    const password = `NewEmployee@${getRandomInt(100000, 100000000)}`;

    const employee = await User.create({
      ...body,
      password: HashHelper.hashPassword(password),
      national_id: `${body.national_id}`,
      status: 'active'
    });

    const mailer = new Mailer({
      to: employee.email,
      html: informNewEmployee({
        name: employee.name,
        email: employee.email,
        password
      }),
      subject: 'You have been invited to Employeemanager'
    });


    await mailer.send();

    res.status(201).json({
      status: 201,
      message: 'A new employee has been created!',
      data: employee
    });
  }

  /**
   *
   * @param {Object} req Request
   * @param {Object} res Response
   * @returns {Object} Response
   */
  static async deleteOne(req, res) {
    const { params, user } = req;

    if (params.employee_id === user.id) {
      return res.status(403).json({
        status: 403,
        message: 'You can not delete yourself'
      });
    }

    const employee = await User.findOne({
      where: {
        id: params.employee_id
      }
    });

    if (!employee) {
      return res.status(400).json({
        status: 400,
        message: 'Employee you want to delete does not exist'
      });
    }

    await User.destroy({
      where: {
        id: params.employee_id
      }
    });

    res.json({
      status: 200,
      message: 'User is successfully removed'
    });
  }

  /**
   *
   * @param {Object} req Request object
   * @param {Object} res Response object
   * @returns {Object} Response object
   */
  static async updateOne(req, res) {
    const {
      body, params, employee
    } = req;
    if (!Object.keys(body).length) {
      return res.status(400).json({
        status: 400,
        message: 'You are not updating anything'
      });
    }

    const where = {
      where: {
        id: params.employee_id
      }
    };

    const bornDate = new Date(body.date_of_birth);
    const today = new Date();

    const years = moment(today).diff(moment(bornDate), 'years');
    if (parseInt(years, 10) < 18) {
      return res.status(400).json({
        status: 400,
        message: 'Unfortunately, this system only accepts people with at least 18 years old'
      });
    }
    await User.update({
      name: body.name || employee.name,
      email: body.email || employee.email,
      national_id: `${body.national_id}` || employee.national_id,
      phone_number: body.phone_number || employee.phoone_number,
      date_of_birth: body.date_of_birth || employee.date_of_birth,
      position: body.position || employee.position
    }, where);

    return res.status(200).json({
      status: 200,
      message: 'An employee has been updated successfully'
    });
  }

  /**
   *
   * @param {Object} req Request
   * @param {Object} res Response
   * @returns {Object} Response object
   */
  static async activateOne(req, res) {
    const { params } = req;

    const where = {
      where: {
        id: params.employee_id
      }
    };

    await User.update({
      status: 'active'
    }, where);

    res.status(200).json({
      status: 200,
      message: 'Employee is now active'
    });
  }

  /**
   *
   * @param {Object} req Request object
   * @param {*} res Response object
   * @returns {Object} Response
   */
  static async suspendOne(req, res) {
    const { params } = req;
    const where = {
      where: {
        id: params.employee_id
      }
    };

    await User.update({
      status: 'inactive'
    }, where);

    res.json({
      status: 200,
      message: 'EMployee is now inactive'
    });
  }

  /**
   *
   * @param {Object} req Request
   * @param {Object} res Response
   * @returns {Object} Response
   */
  static async search(req, res) {
    const { query } = req;
    const where = {
      where: {
        [Op.or]: [
          {
            email: {
              [Op.iLike]: `%${query.q}%`
            }
          },
          {
            phone_number: {
              [Op.iLike]: `%${query.q}%`
            }
          },
          {
            national_id: {
              [Op.iLike]: `%${query.q}%`
            }
          },
          {
            position: {
              [Op.iLike]: `%${query.q}%`
            }
          },
          {
            name: {
              [Op.iLike]: `%${query.q}%`
            }
          }
        ]
      }
    };

    const employees = await User.findAll(where);

    return res.json({
      status: 200,
      data: employees
    });
  }
}

export default EmployeeController;
