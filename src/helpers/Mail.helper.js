import sendgrid from '@sendgrid/mail';
import 'dotenv/config';
/**
 * @class Mailer
 */
class Mailer {
  /**
     *
     * @param {String} to -Recipient
     * @param {String} html - Html message
     * @param {String} subject - Subjecy
     */
  constructor({ to, html, subject }) {
    this.to = to;
    this.html = html;
    this.subject = subject;
    sendgrid.setApiKey(process.env.SENDGRID_API_KEY);
  }

  /**
   * @returns {Promise} - A send mail promise
   */
  async send() {
    try {
      const msg = {
        to: this.to,
        from: 'Account <account@employeemanager.com>',
        subject: this.subject,
        html: this.html
      };
      const result = await sendgrid.send(msg);
      return result;
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error.response.body.errors);
      return error;
    }
  }
}

export default Mailer;
