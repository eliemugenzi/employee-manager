import { Joi } from 'celebrate';

export const employeeCreationRule = Joi.object().keys({
  name: Joi.string().required().label('Name is required'),
  national_id: Joi.number().integer().min(16).required()
    .label('National ID must be at least 16 characters'),
  phone_number: Joi.string().regex(/^\d{3}-\d{3}-\d{3}-\d{3}$/).required().example('0785-123-456')
    .label('Phone number is required and must be in this format: 250-785-019-278'),
  date_of_birth: Joi.date().required().label('Date of birth is required and must be in this format 01-01-2020'),
  email: Joi.string().email().required().label('Email is required'),
  position: Joi.string().required().label('Position is required')
});

export const employeeSearchRule = Joi.object().keys({
  query: Joi.string().required().label('Query keyword is required')
});

export const employeeByIdRule = Joi.object().keys({
  employee_id: Joi.number().integer().required().label('Employee ID should be provided and must be an integer')
});

export const employeeUpdataRule = Joi.object().keys({
  name: Joi.string().label('Name must be a string'),
  national_id: Joi.number().integer().min(16)
    .label('National ID must be at least 16 characters'),
  phone_number: Joi.string().regex(/^\d{3}-\d{3}-\d{3}-\d{3}$/).example('0785-123-456')
    .label('Phone number must be in this format: 250-785-019-278'),
  date_of_birth: Joi.date().label('Date of birth must be in this format 01-01-2020'),
  email: Joi.string().email().label('Email muust be in the correct format'),
  position: Joi.string().label('Position must be a string')
});
