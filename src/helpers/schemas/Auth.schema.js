import { Joi } from 'celebrate';

export const signupRule = Joi.object().keys({
  name: Joi.string().required().label('Name is required'),
  national_id: Joi.number().integer().min(16)
    .required()
    .label('National ID is required and must have at least 16 characters'),
  phone_number: Joi.string().regex(/^\d{3}-\d{3}-\d{3}-\d{3}$/).required().example('250-785-844-487')
    .label('Phone number is required and should be in this format: 250-788-097-486'),
  date_of_birth: Joi.date().required().label('Date of birth must be in this format: 01-01-2020'),
  password: Joi.string().regex(/^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/).required().label('Password must have at least an uppercase, lowercase, integer and special character'),
  confirm_password: Joi.any().required().valid(Joi.ref('password')).label('Passwords must match'),
  email: Joi.string().email().required().label('Email is required')
});

export const loginRule = Joi.object().keys({
  email: Joi.string().email().required().label('Email is required and must be in the correct format'),
  password: Joi.string().required().label('Password is required')
});
