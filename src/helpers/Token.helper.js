import jwt from 'jsonwebtoken';
import 'dotenv/config';

/**
 * @class Tokenizer
 */
class Tokenizer {
  /**
     *
     * @param {Object} payload _User Information
     * @returns {String} JWT token
     */
  static async generateToken(payload) {
    const token = await jwt.sign(payload, process.env.SECRET_KEY);
    return token;
  }

  /**
   *
   * @param {String} token JWT token
   * @returns {Object} User object
   */
  static async decodeToken(token) {
    const payload = await jwt.decode(token, process.env.SECRET_KEY);
    return payload;
  }
}

export default Tokenizer;
