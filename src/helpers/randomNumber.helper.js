
/**
 *
 * @param {Number} min Minimum
 * @param {*} max Maximum
 * @returns {Number} Number
 */
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

export default getRandomInt;
