import bcrypt from 'bcrypt';

/**
 * @class HashHelper
 */
class HashHelper {
  /**
     *
     * @param {String} password - Password to hash
     * @returns {String} hashedPassword
     */
  static hashPassword(password) {
    const hashedPassword = bcrypt.hashSync(password, 10);
    return hashedPassword;
  }

  /**
   *
   * @param {String} password - Real Password
   * @param {String} hashedPassword - Hashed Password
   * @returns {Boolean} True is they match, false otherwise
   */
  static comparePassword(password, hashedPassword) {
    return bcrypt.compareSync(password, hashedPassword);
  }
}

export default HashHelper;
