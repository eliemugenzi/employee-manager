import 'dotenv/config';

const { API_URL } = process.env;

const informNewEmployee = ({
  name,
  email,
  password
}) => {
  const template = `
      <div>
        <h1>Hi ${name}, Welcome to Employee Manager platform</h1>
        <p>We can confirm that you have been added to our system. Check the credentials below and login </p>
        <strong>Email: </strong> ${email} <br />
        <strong>Password: </strong> ${password} <br />
        <a href='${API_URL}/login'>Login</a>

      </div>
    `;

  return template;
};

export default informNewEmployee;
