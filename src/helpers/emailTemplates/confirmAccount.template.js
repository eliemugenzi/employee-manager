import 'dotenv/config';

const { API_URL } = process.env;
const confirmAccountTemplate = ({
  name, email, token
}) => {
  const template = `
   <div>
     <h1>Welcome to the platform, ${name}.</h1>
     <p>To confirm if ${email} is yours, please click on the following link</p>
     <a href='${API_URL}/auth/verify/?token=${token}'>Verify Account</a>
    </div>

   
   `;

  return template;
};

export default confirmAccountTemplate;
